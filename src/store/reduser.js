import {
  INCREMENT, DECREMENT, ADD, SUBTRACT, FETCH_COUNTER_SUCCESS, INCREMENT_SUCCESS,
  REQUEST, INCREMENT_ERROR, DECREMENT_SUCCESS, DECREMENT_ERROR, ADD_COUNTER_SUCCESS, ADD_COUNTER_ERROR,
  SUBTRACT_COUNTER_SUCCESS, SUBTRACT_COUNTER_ERROR, FETCH_COUNTER_ERROR
} from './actions';



const initialState = {
  counter: 0,
  loading: false,
  hasError: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) { // создает новый state на основании action-(ов)
    case REQUEST:
      return {...state, loading: true};
    case ADD:
      return {counter: state.counter + action.amount};
    case ADD_COUNTER_SUCCESS:
      return {counter: action.counter.counter ,loading: false};
    case ADD_COUNTER_ERROR:
      return {...state, loading: false, hasError: true};
    case SUBTRACT:
      return {counter: state.counter - action.amount};
    case SUBTRACT_COUNTER_SUCCESS:
      return {counter: action.counter.counter ,loading: false};
    case SUBTRACT_COUNTER_ERROR:
      return {...state, loading: false, hasError: true};
    case FETCH_COUNTER_SUCCESS:
      return {counter: action.counter.counter};
    case FETCH_COUNTER_ERROR:
      return {...state, loading: false, hasError: true};
    case INCREMENT:
      return {counter: state.counter + 1};
    case INCREMENT_SUCCESS:
      return {counter: action.counter.counter, loading: false};
    case INCREMENT_ERROR:
      return {...state, loading: false, hasError: true};
    case DECREMENT:
      return {counter: state.counter - 1};
    case DECREMENT_SUCCESS:
      return {...state, counter: action.counter.counter, loading: false};
    case DECREMENT_ERROR:
      return {...state, loading: false, hasError: true};
    default:
      return state;
  }
};

export default reducer;