import axios from '../axios-counter';
// нужно всегда делать 1-ми импорты а потом экспорты, иначе будут проблемы
export const REQUEST = 'REQUEST';
export const INCREMENT = 'INCREMENT';
export const INCREMENT_SUCCESS = 'INCREMENT_SUCCESS';
export const INCREMENT_ERROR = 'INCREMENT_ERROR';
export const DECREMENT = 'DECREMENT';
export const DECREMENT_SUCCESS = 'DECREMENT_SUCCESS';
export const DECREMENT_ERROR = 'DECREMENT_ERROR';
export const ADD = 'ADD';
export const ADD_COUNTER_SUCCESS = 'ADD_COUNTER_SUCCESS';
export const ADD_COUNTER_ERROR = 'ADD_COUNTER_ERROR';
export const SUBTRACT = 'SUBTRACT';
export const SUBTRACT_COUNTER_SUCCESS = 'SUBTRACT_COUNTER_SUCCESS';
export const SUBTRACT_COUNTER_ERROR = 'SUBTRACT_COUNTER_ERROR';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

// способ организации action-(ов)

export const request = () => {
  return {type: REQUEST};
};

export const increment = counter => dispatch => {
  dispatch(request());
  axios.patch('/counter.json', {counter: counter + 1})
    .then(response => dispatch(incrementSuccess(response.data)))
    .catch(() => dispatch(incrementError()));
};

export const incrementSuccess = counter => {
  return {type: INCREMENT_SUCCESS, counter};
};

export const incrementError = () => {
  return {type: INCREMENT_ERROR};
};

export const decrement = counter => dispatch => {
  dispatch(request());
  axios.patch('/counter.json', {counter: counter - 1})
    .then(response => dispatch(decrementSuccess(response.data)))
    .catch(() => dispatch(decrementError()));
};

export const decrementSuccess = counter => {
  return {type: DECREMENT_SUCCESS, counter};
};

export const decrementError = counter => {
  return {type: DECREMENT_ERROR, counter};
};

export const addCounter = (counter, amount) => dispatch => {
  dispatch(request());
  axios.patch('/counter.json', {counter: counter + amount})
    .then(response => dispatch(addCounterSuccess(response.data)))
    .catch(() => dispatch(addCounterError()));
};

export const addCounterSuccess = counter => {
  return {type: ADD_COUNTER_SUCCESS, counter};
};

export const addCounterError = () => {
  return {type: ADD_COUNTER_ERROR};
};

export const subtractCounter = (counter, amount) => dispatch => {
  dispatch(request());
  axios.patch('/counter.json', {counter: counter - amount})
    .then(response => dispatch(subtractCounterSuccess(response.data)))
    .catch(() => dispatch(subtractCounterError()));
};

export const subtractCounterSuccess = counter => {
  return {type: ADD_COUNTER_SUCCESS, counter};
};

export const subtractCounterError = () => {
  return {type: ADD_COUNTER_ERROR};
};

export const fetchCounter = () => {
  // цель отправки функций через dispatch - это в итоге получение JS объекта
  // который будет отправлен в reduser
  // Redux не понимает функции, а понимает только JS объекты и когда функция
  // отправляется в redux - thunk ее перехватывает и выполняется thunk(ом)
  // в результате которой в redux должен отправиться понятный ему action.
  // thunk это перехватчик как interceptor чтобы redux понял что
  // отправляется функция.
  return dispatch => {
    dispatch(request());
    axios.get('/counter.json')
      .then(response => dispatch(fetchCounterSuccess(response.data)))
      .catch(() => dispatch(fetchCounterError()));
  }
};

export const fetchCounterSuccess = (counter) => {
  return {type: FETCH_COUNTER_SUCCESS, counter};
};

export const fetchCounterError = () => {
  return {type: FETCH_COUNTER_ERROR};
};


