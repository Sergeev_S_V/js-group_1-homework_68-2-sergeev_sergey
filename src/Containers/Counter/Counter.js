import React, {Component} from 'react';
import {connect} from 'react-redux'; // возвращает функцию если вызвать connect

import './Counter.css';
import {
  addCounter, decrement, decrementCounter, fetchCounter, increment,
  subtractCounter
} from "../../store/actions";
import Spinner from "../../hoc/Spinner/Spinner";


class Counter extends Component {
  componentDidMount() {
    this.props.fetchCounter();
  };

  render() {
    let errorMessage;
    if (this.props.hasError) errorMessage = <div>Error!!!</div>;
    return(
      <div className='Counter'>
        {errorMessage}
        <Spinner loading={this.props.loading}/>
        <h1>{this.props.ctr}</h1>
        <button onClick={() => this.props.increaseCounter(this.props.ctr)}>Increase</button>
        <button onClick={() => this.props.decreaseCounter(this.props.ctr)}>Decrease</button>
        <button onClick={() => this.props.addCounter(this.props.ctr)}>Increase by 5</button>
        <button onClick={() => this.props.subtractCounter(this.props.ctr)}>Decrease by 5</button>
      </div>
    );
  }
}

const mapStateToProps = state => { // принимает только state и возвращать все props которые нам нужно в этом компоненте
  return { // этот метод нужно только чтобы просто брать вещи из state.
    ctr: state.counter,// ctr - это название будущего props(a)
    loading: state.loading,
    hasError: state.hasError,
  };
};

const mapDispatchToProps = dispatch => { // принимает только dispatch
  return { // то что здесь вернет тоже добавится в наши props(ы)
    increaseCounter: counter => dispatch(increment(counter)), // может содержать небольшие вычисления
    decreaseCounter: counter => dispatch(decrement(counter)),
    addCounter: counter => dispatch(addCounter(counter, 5)),
    subtractCounter: counter => dispatch(subtractCounter(counter, 5)),
    fetchCounter: () => dispatch(fetchCounter()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter); // connect принимает две функции
