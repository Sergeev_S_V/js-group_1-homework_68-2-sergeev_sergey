const redux = require('redux'); // импорт все что экспортирует по default -> redux

const initialState = {
  counter: 0,
};

const rootReducer = (state = initialState, action) => {// Редьюсер(похож на reduce)
  if (action.type === 'INC_COUNTER') { // в процессе работы reducer не должны менять в нем ничего
    return {...state, counter: state.counter + 1}; // нужно работать только с копией в reducer
  }
  if (action.type === 'ADD_COUNTER') {
    return {...state, counter: state.counter + action.value}
  }
  return state;
};

const store = redux.createStore(rootReducer); // Запускается 1-н раз при создании store
store.subscribe(() => {
  console.log('[Subscriber]', store.getState());
});

console.log('before:', store.getState());

store.dispatch({type: 'INC_COUNTER'}); // <-- это action который объект(все что передаем в dispatch). type обязательно
store.dispatch({type: 'ADD_COUNTER', value: 10}); // после того как есть type - можно передавать value
store.dispatch({type: 'INC_COUNTER'});

console.log('after:', store.getState());
